from rest_framework import serializers, status
from django.db import transaction

from .models import *


class ActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actor
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = '__all__'


class ListNotMovieListSerializer(serializers.ModelSerializer):
    class Meta:
        model = List
        fields = ('id', 'name',)


class MovieSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(read_only=True, many=True)
    actors = ActorSerializer(read_only=True, many=True)
    list = ListNotMovieListSerializer(read_only=True, many=False)

    class Meta:
        model = Movie
        fields = '__all__'


class ListSerializer(serializers.ModelSerializer):
    movies = MovieSerializer(read_only=True, many=True)

    class Meta:
        model = List
        fields = '__all__'
