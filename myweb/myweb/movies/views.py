
from django.db.models import Prefetch

import django_filters

from rest_framework import permissions
from rest_framework.request import QueryDict
from rest_framework.viewsets import ModelViewSet

from .serializers import *
from .models import *


class ActorFilter(django_filters.FilterSet):
    class Meta:
        model = Actor
        fields = {
            'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
        }


class ActorViewSet(ModelViewSet):
    queryset = Actor.objects.all().order_by('id')
    serializer_class = ActorSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_class = ActorFilter


class GenreFilter(django_filters.FilterSet):
    class Meta:
        model = Genre
        fields = {
            'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
        }


class GenreViewSet(ModelViewSet):
    queryset = Genre.objects.all().order_by('id')
    serializer_class = GenreSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_class = GenreFilter


class ListFilter(django_filters.FilterSet):
    class Meta:
        model = List
        fields = {
            'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
        }


class MovieFilter(django_filters.FilterSet):
    class Meta:
        model = Movie
        fields = {
            'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
            'director': ['icontains', 'exact', 'istartswith', 'iendswith'],
            'year': ['icontains', 'exact', 'istartswith', 'iendswith'],
        }


class MovieViewSet(ModelViewSet):
    queryset = Movie.objects.all().order_by('id').distinct()
    serializer_class = MovieSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_class = MovieFilter


class ListViewSet(ModelViewSet):
    queryset = List.objects.all().order_by('id')
    serializer_class = ListNotMovieListSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_class = ListFilter


