from django.contrib import admin

# Register your models here.
from .models import Actor, Genre, List, Movie

admin.site.register(Actor)
admin.site.register(Genre)
admin.site.register(List)
admin.site.register(Movie)