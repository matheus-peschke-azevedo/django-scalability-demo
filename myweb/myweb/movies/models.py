from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Actor(models.Model):
    imdbid = models.CharField(max_length=7, unique=True)
    name = models.CharField(max_length=200, db_index=True)

    def __str__(self):
        return "Actor(ess): {}".format(self.name)


class Genre(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return "Genre: {}".format(self.name)


class List(models.Model):
    name = models.CharField(max_length=8, unique=True)
    
    def __str__(self):
        return "List: {}".format(self.name)


class Movie(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    director = models.CharField(max_length=215, db_index=True)
    imdb = models.CharField(max_length=70)
    year = models.CharField(max_length=4)

    list = models.ForeignKey(List, related_name="movies", on_delete=models.CASCADE)

    genres = models.ManyToManyField(Genre, through='GenreInMovie')
    actors = models.ManyToManyField(Actor, through='ActorInMovie')

    def __str__(self):
        return "Movie: {}".format(self.name)


class ActorInMovie(models.Model):
    actor = models.ForeignKey(Actor, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return "ActorInMovie: {}".format(self.actor)

    class Meta:
        db_table = "movies_movie_actors"


class GenreInMovie(models.Model):
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return "GenreInMovie: {}".format(self.genre)

    class Meta:
        db_table = "movies_movie_genres"
