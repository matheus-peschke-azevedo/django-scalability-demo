from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'lists', ListViewSet)
router.register(r'movies', MovieViewSet)
router.register(r'actors', ActorViewSet)
router.register(r'genres', GenreViewSet)

urlpatterns = router.urls
