from locust import HttpLocust, TaskSet, task
import locust.stats

locust.stats.CSV_STATS_INTERVAL_SEC = 5

class LoadAndStress(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.login()

    def login(self):
        response = self.client.get('/admin/login/')
        csrftoken = response.cookies['csrftoken']
        self.client.post('/admin/login/',
                         {'username': 'movies', 'password': 'movies1234'}, 
                         headers={'X-CSRFToken': csrftoken})
    @task
    def movies(self):
        self.client.get("/v1.0/movies/")

    @task
    def actors(self):
        self.client.get("/v1.0/actors/")

class LibraryUser(HttpLocust):
    task_set = LoadAndStress
    min_wait = 200
    max_wait = 500


