# README #

Bem vindo ao treino de Django escalável!

### Qual é o propósito desse repo? ###

* Sumário

Um treinamento sobre Django, com um viés DevOps de stressing de serviços (requisitos não funcionais)

* Versão
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Setup de desenvolvimento ###

* Sumário

Procure pelo vídeo de treinamento no Slack da Sciensa: https://sciensa.slack.com/messages/G2SSCDPDJ/convo/G2SSCDPDJ-1517325159.000426/

* Configuração

1. Ubuntu 16.04
2. Visual Studio Code com Python

----------------------------------------------------------------
- Iniciando o repositório
----------------------------------------------------------------

1. Execute os comandos:

$git init

$git config user.name "matheus-peschke-azevedo"

$git config user.email "matheus.azevedo@soaexpert.com.br"

$git remote add origin https://matheus-peschke-azevedo@bitbucket.org/matheus-peschke-azevedo/django-scalability-demo.git

$git fetch

$git checkout -b develop --track origin/develop

----------------------------------------------------------------
- Configurando máquina de desenvolvimento
----------------------------------------------------------------

2. Abrir Visual Studio Code e mudar para o venv python interpreter!

$/usr/bin/python3 -m venv .venv
$source .venv/bin/activate

----------------------------------------------------------------
- Instalando os pacotes pip
----------------------------------------------------------------

3. Execute os comandos:

#apt-get install -y wget python-dev python3-dev libmysqlclient-dev python3-pip python3-venv

$pip3 install --upgrade pip

$pip3 install 'wheel==0.30.0' --force-reinstall

$pip3 install 'mysqlclient==1.3.12' --force-reinstall

$pip3 install 'django==2.0' --force-reinstall

$pip3 install 'djangorestframework==3.7.3' --force-reinstall

$pip3 install 'django-filter==1.1.0' --force-reinstall

### Sobre como ajudar nesse projeto ###

* TODO

### Histórico - Como foi criado esse projeto Movies ###

----------------------------------------------------------------
- Criando a app movies
----------------------------------------------------------------

5. Criando o app movies

$python manage.py startapp movies

(Veja no mysql que as tabelas não existem)

6. Codificando os models - myweb/myweb/movies/models.py

    from django.db import models

    class Actor(models.Model):
        imdbid = models.CharField(max_length=7, unique=True)
        name = models.CharField(max_length=200, db_index=True)

        def __str__(self):
            return "Actor(ess): {}".format(self.name)


    class Genre(models.Model):
        name = models.CharField(max_length=20, unique=True)

        def __str__(self):
            return "Genre: {}".format(self.name)


    class List(models.Model):
        name = models.CharField(max_length=8, unique=True)
        
        def __str__(self):
            return "List: {}".format(self.name)


    class Movie(models.Model):
        name = models.CharField(max_length=100, db_index=True)
        director = models.CharField(max_length=215, db_index=True)
        imdb = models.CharField(max_length=70)
        year = models.CharField(max_length=4)

        list = models.ForeignKey(List, related_name="movies", on_delete=models.CASCADE)

        genres = models.ManyToManyField(Genre, through='GenreInMovie')
        actors = models.ManyToManyField(Actor, through='ActorInMovie')

        def __str__(self):
            return "Movie: {}".format(self.name)


    class ActorInMovie(models.Model):
        actor = models.ForeignKey(Actor, on_delete=models.CASCADE)
        movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

        def __str__(self):
            return "ActorInMovie: {}".format(self.actor)

        class Meta:
            db_table = "movies_movie_actors"


    class GenreInMovie(models.Model):
        genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
        movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

        def __str__(self):
            return "GenreInMovie: {}".format(self.genre)

        class Meta:
            db_table = "movies_movie_genres"



7. Adicione a aplicação movies e dependências à lista em settings.py:

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'rest_framework',
        'django_filters',
        'movies'
    ]

    REST_FRAMEWORK = {
        'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
        'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
        'PAGE_SIZE': 5
    }

8. Gerando as migrações

(gera os scripts de migração)
$python manage.py makemigrations

(roda os scripts de migração)
$python manage.py migrate

(mostrar as tabelas criadas)

9. Criando o superusuário (para acesso à administração do backend).

Em admin.py do app movies:

    from .models import Actor, Genre, List, Movie

    admin.site.register(Actor)
    admin.site.register(Genre)
    admin.site.register(List)
    admin.site.register(Movie)

10. Rode:

$python manage.py createsuperuser

Acesse:

$python manager.py runserver
http://127.0.0.1:8000/admin/

11. Criando serializers (json) e as urls do RESTfull webservice

a. Em serializers.py (criar):

    from rest_framework import serializers, status
    from django.db import transaction

    from .models import *


    class ActorSerializer(serializers.ModelSerializer):
        class Meta:
            model = Actor
            fields = '__all__'


    class GenreSerializer(serializers.ModelSerializer):
        class Meta:
            model = Genre
            fields = '__all__'


    class ListNotMovieListSerializer(serializers.ModelSerializer):
        class Meta:
            model = List
            fields = ('id', 'name',)


    class MovieSerializer(serializers.ModelSerializer):
        genres = GenreSerializer(read_only=True, many=True)
        actors = ActorSerializer(read_only=True, many=True)
        list = ListNotMovieListSerializer(read_only=True, many=False)

        class Meta:
            model = Movie
            fields = '__all__'


    class ListSerializer(serializers.ModelSerializer):
        movies = MovieSerializer(read_only=True, many=True)

        class Meta:
            model = List
            fields = '__all__'


b. Em views.py:

    from django.db.models import Prefetch

    import django_filters

    from rest_framework import permissions
    from rest_framework.request import QueryDict
    from rest_framework.viewsets import ModelViewSet

    from .serializers import *
    from .models import *


    class ActorFilter(django_filters.FilterSet):
        class Meta:
            model = Actor
            fields = {
                'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
            }


    class ActorViewSet(ModelViewSet):
        queryset = Actor.objects.all().order_by('id')
        serializer_class = ActorSerializer
        permission_classes = (permissions.IsAuthenticated,)
        filter_class = ActorFilter


    class GenreFilter(django_filters.FilterSet):
        class Meta:
            model = Genre
            fields = {
                'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
            }


    class GenreViewSet(ModelViewSet):
        queryset = Genre.objects.all().order_by('id')
        serializer_class = GenreSerializer
        permission_classes = (permissions.IsAuthenticated,)
        filter_class = GenreFilter


    class ListFilter(django_filters.FilterSet):
        class Meta:
            model = List
            fields = {
                'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
            }


    class MovieFilter(django_filters.FilterSet):
        class Meta:
            model = Movie
            fields = {
                'name': ['icontains', 'exact', 'istartswith', 'iendswith'],
                'director': ['icontains', 'exact', 'istartswith', 'iendswith'],
                'year': ['icontains', 'exact', 'istartswith', 'iendswith'],
            }


    class MovieViewSet(ModelViewSet):
        queryset = Movie.objects.all().order_by('id').distinct()
        serializer_class = MovieSerializer
        permission_classes = (permissions.IsAuthenticated,)
        filter_class = MovieFilter


    class ListViewSet(ModelViewSet):
        queryset = List.objects.all().order_by('id')
        serializer_class = ListNotMovieListSerializer
        permission_classes = (permissions.IsAuthenticated,)
        filter_class = ListFilter



d. Em urls.py (criar):

    from .views import *
    from rest_framework.routers import DefaultRouter

    router = DefaultRouter()
    router.register(r'lists', ListViewSet)
    router.register(r'movies', MovieViewSet)
    router.register(r'actors', ActorViewSet)
    router.register(r'genres', GenreViewSet)

    urlpatterns = router.urls

e. Em MYWEB/urls.py:

    from django.conf.urls import url, include
    from django.contrib import admin
    from django.views.generic import TemplateView

    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^v1.0/', include('movies.urls')),
    ]

12. Testando as mensagens json usando curl:

$python manage.py runserver

$curl -H 'Accept: application/json; indent=4' localhost:8000/v1.0/
$curl -H 'Accept: application/json; indent=4' -u movies:movies1234 localhost:8000/v1.0/lists/

-----------------------------------------------------------
TESTES DE STRESS
-----------------------------------------------------------

13. Instale o locust pip package (https://locust.io/):
http://blog.apcelent.com/load-test-django-application-using-locustio.html

$pip3 install locustio

14. Criando o arquivo de teste de stress

a. Crie em movies/locustfile.py

    from locust import HttpLocust, TaskSet, task
    import locust.stats

    locust.stats.CSV_STATS_INTERVAL_SEC = 5

    class LoadAndStress(TaskSet):
        def on_start(self):
            """ on_start is called when a Locust start before any task is scheduled """
            self.login()

        def login(self):
            response = self.client.get('/admin/login/')
            csrftoken = response.cookies['csrftoken']
            self.client.post('/admin/login/',
                            {'username': 'movies', 'password': 'movies1234'}, 
                            headers={'X-CSRFToken': csrftoken})
        @task
        def movies(self):
            self.client.get("/v1.0/movies/")

        @task
        def actors(self):
            self.client.get("/v1.0/actors/")

    class LibraryUser(HttpLocust):
        task_set = LoadAndStress
        min_wait = 50
        max_wait = 100


15. Executando o load de dados e os testes de stress

a. Dê um load de dados no banco, usando o script em data/movies.sql. É um
arquivo sql self-contained, você pode usar o comando 'import' do Workbench, por
exemplo, para fazer a importação.

b. Em myweb/movies/movies, executar o comando abaixo.
--no-web: não inicia a tela de administração do testes - teste automatizável
-c 10: use até 10 usuários logados no teste.
-r 1: incremente o número de usuários (1 por segundo) no teste.
-n 1000: execute 1000 requisições (total)

$locust --host=http://localhost:8000 --csv=stressresults --no-web -c 10 -r 1 -n 1000

### Contatos sobre esse repo ###

* Matheus Peschke de Azevedo (matheus.azevedo@sciensa.com)